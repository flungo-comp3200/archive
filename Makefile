MAVEN_GOALS = install

all: parent flungo-parent source

.PHONY: parent flungo-parent source

flungo-parent: parent
parent flungo-parent:
	cd $@ && mvn $(MAVEN_CLI_OPTS) install

source: flungo-parent
	cd $@ && mvn $(MAVEN_CLI_OPTS) site
